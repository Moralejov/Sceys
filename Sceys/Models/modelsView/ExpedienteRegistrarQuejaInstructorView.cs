﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sceys.Models.modelsView
{
    public class ExpedienteRegistraQuejaXInstructorView
    {
        //Información para tabla Expedientes      
        public string documentoAprendiz { get; set; }               
        public string descripcionQueja { get; set; }

        //Información para relación con otras tablas
        public int tipoFaltaId { get; set; }
        public int calificacionId { get; set; }       
    }
}