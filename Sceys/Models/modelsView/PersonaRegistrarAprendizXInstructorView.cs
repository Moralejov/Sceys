using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class PersonaRegistrarAprendizXInstructorView
    {
        public int TipoPersonaId { get; set; }

        public int TipoDocumento { get; set; }

        public string Documento { get; set; }

        public string PrimerNombre { get; set; }

        public string SegundoNombre { get; set; }

        public string PrimerApellido { get; set; }

        public string SegundoApellido { get; set; }

        public string Correo {get;set;}

        public string Direccion {get;set;}

        public string Celular {get;set;}

        public string Telefono {get;set;}

        public string Ficha {get;set;}

        public int RegionalId {get;set;}

        public int CentroId {get;set;}
    }
}
