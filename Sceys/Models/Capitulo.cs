﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Capitulo
    {
        [Key]
        public int CapituloId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(8000, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 1)]
        [Display(Name = "Capitulo")]
        public string Descripcion { get; set; }

        public virtual ICollection<Articulo> Articulos { get; set; }
    }
}