﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class TipoDocumento
    {
        [Key]
        public int TipoDocumentoId { get; set; }

        //[Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(100, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Tipo Documento")]
        public string Descripcion { get; set; }


        public string Abreviacion { get; set; }
        
        public virtual ICollection<Persona> Personas { get; set; }
    }
}