﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Informante
    {
        [Key]
        public int InformanteId { get; set; }

        public int PersonaId { get; set; }

        public string Descripcion { get; set; }


        //virtuals models
        public virtual Persona Persona { get; set; }

        //ICollection
       
    }
}