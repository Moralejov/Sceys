﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Cita
    {
        [Key]
        public int CitaId { get; set; }

        [Display(Name = "Articulo")]
        public int ArticuloId { get; set; }

        [StringLength(1000, ErrorMessage = "{0} debe tener máximo {1} caracteres", MinimumLength = 1)]
        [Display(Name = "Titulo Cita")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(8000, ErrorMessage = "{0} debe tener máximo {1} caracteres", MinimumLength = 1)]
        [Display(Name = "Cita")]
        public string Descripcion { get; set; }

        //virtual
        public virtual Articulo Articulo { get; set; }
    }
}