﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sceys.Models
{
    public class TipoPersona
    {
        [Key]
        public int TipoPersonaId { get; set; }

        //[Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(100, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Tipo de persona")]
        public string Descripcion { get; set; }

        public virtual ICollection<Persona> Personas { get; set; }
    }
}
