﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class RecepcionDocumento
    {
        [Key]
        public int RecepcionDocumentoId { get; set; }

        public int ExpedienteId { get; set; }

        public int PersonaId { get; set; }

        public string Tipo { get; set; }

        // virtual models        
        public virtual Expediente Expediente { get; set; }

        public virtual Persona Persona { get; set; }

        //ICollection        
    }
}