﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Linq;
using System.Web;


namespace Sceys.Models
{
    public class ArticuloFalta
    {
        [Key]
        public int ArticuloFaltaId { get; set; }

        public int ArticuloId { get; set; }

        public int FaltaId { get; set; }       

        //virtual
        public virtual Articulo Articulo { get; set; }
        public virtual Falta Falta { get; set; }

    }
}