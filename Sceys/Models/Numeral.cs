﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sceys.Models
{
    public class Numeral
    {
        [Key]
        public int NumeralId { get; set; }

        [Display(Name = "Articulo")]
        public int ArticuloId { get; set; }

        [StringLength(1000, ErrorMessage = "{0} debe tener máximo {1} caracteres", MinimumLength = 1)]
        [Display(Name = "Titulo Numeral")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(8000, ErrorMessage = "{0} debe tener máximo {1} caracteres", MinimumLength = 1)]
        [Display(Name = "Numeral")]
        public string Descripcion { get; set; }

        //virtual
        public virtual Articulo Articulo { get; set; }

        //relations
        public ICollection<SubNumeral> SubNumeral { get; set; }
    }
}
