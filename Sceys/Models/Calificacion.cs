﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Calificacion
    {
        [Key]
        public int CalificacionId { get; set;}

        public string Descripcion { get; set; }


        //relations
        public ICollection<Falta> Falta { get; set; }
    }
}