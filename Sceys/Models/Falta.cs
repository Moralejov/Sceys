﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Sceys.Models
{
    public class Falta
    {
        [Key]
        public int FaltaId { get; set; }

        public int ExpedienteId { get; set; }

        public int TipoFaltaId { get; set; }

        public int CalificacionId { get; set; }

        public string DescripcionFaltas { get; set; }


        //model virtual 
        public virtual Expediente Expediente { get; set; }
        public virtual TipoFalta TipoFalta { get; set; }
        public virtual Calificacion Calificacion { get; set; }

        //relations
        public virtual ICollection<ArticuloFalta> ArticuloFalta { get; set; }

    }
}