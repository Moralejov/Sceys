﻿$(document).ready(function () {
    $('#pdf').on('click', function () {
        //Contenido del expediente

        // var aprendiz = $("#aprendiz").text();
        var contenido = "loren ipsu";
        var urly = '@Url.Content("~/Imagen/SENA-logo.png")';

        //mostrar pdf en IFRAME
        setTimeout(function () {


            // playground requires you to assign document definition to a variable called dd

            var docDefinition = {
                content: [
                    {
                        stack: [
                                 'REPORTE FALTA ACADÉMICA Y/O DISCIPLINARIA \n',
                                 'F002 P003-8 VERSIÓN 001 \n',
                                 'Proceso: Gestión de la Formación Profesional Integral \n',
                                 'Procedimiento: Gestión de Proyectos Formativos',
                                 '\nFecha: ' + $.datepicker.formatDate('yy/mm/dd', new Date())

                        ],
                        style: 'header'
                    },
                    {
                        text: [
                            'Tipo de falta: ' + 'Académica. \n' + 'Calificación: ' + 'Leve \n\n',
                            'Nombre del aprendiz: \n',
                            'Documento de identidad: \n',
                            'Especialidad: \n',
                            'Formación y código: \n'
                        ]
                    },
                    {
                        text: 'This paragraph (consisting of a single line) directly sets top and bottom margin to 20',
                        margin: [0, 20],
                    },
                    {
                        stack: [
                            {
                                text: [
                                      'This line begins a stack of paragraphs. The whole stack uses a ',
                                      { text: 'superMargin', italics: true },
                                      ' style (with margin and fontSize properties).',
                                ]
                            },
                            { text: ['When you look at the', { text: ' document definition', italics: true }, ', you will notice that fontSize is inherited by all paragraphs inside the stack.'] },
                            'Margin however is only applied once (to the whole stack).'
                        ],
                        style: 'superMargin'
                    },
                    {
                        stack: [
                            'I\'m not sure yet if this is the desired behavior. I find it a better approach however. One thing to be considered in the future is an explicit layout property called inheritMargin which could opt-in the inheritance.\n\n',
                            {
                                fontSize: 15,
                                text: [
                                    'Currently margins for ',
                                    /* the following margin definition doesn't change anything */
                                    { text: 'inlines', margin: 20 },
                                    ' are ignored\n\n'
                                ],
                            },
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                        ],
                        margin: [0, 20, 0, 0],
                        alignment: 'justify'
                    }
                ],
                styles: {
                    header: {
                        fontSize: 10,
                        bold: true,
                        alignment: 'right',
                        margin: [0, 10, 0, 80]
                    },
                    subheader: {
                        fontSize: 14
                    },
                    superMargin: {
                        margin: [20, 0, 40, 0],
                        fontSize: 15,
                    }
                }
            }
            pdfMake.createPdf(docDefinition).open();
        }, 5);
    });
});