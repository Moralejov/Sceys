﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sceys.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Sceys.Controllers
{
    public class PersonasController : Controller
    {
        private SceysContext db = new SceysContext();

        // GET: Personas
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated) {
                var personas = db.Personas.Include(p => p.TipoDocumento).Include(p => p.TipoPersona).Where(p => p.Estado == true).OrderBy(p=>p.TipoPersona.TipoPersonaId);
                return View(personas.ToList());
            } else {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        public JsonResult ListarCentros(int id)
        {
            var Centros = db.Centros.Include("Regional").Where(x => x.RegionalId == id).OrderBy(c => c.Nombre).ToList();
            return Json(Centros, JsonRequestBehavior.AllowGet);

        }

        // GET: Personas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Persona personas = db.Personas.Find(id);
            TipoPersona tipoPersona = db.TipoPersonas.Find(personas.TipoPersonaId);
            TipoDocumento tipoDocumento = db.TipoDocumentos.Find(personas.TipoDocumentoId);
            if (personas == null)
            {
                return HttpNotFound();
            }
            var personaViewEdit = new PersonaViewEdit
            {
                AprendizId = 0,
                Celular = personas.Celular,
                CentroId = 0,
                Correo = personas.Correo,
                Direccion = personas.Direccion,
                Documento = personas.Documento,
                Ficha = null,
                PersonaId = personas.PersonaId,
                PrimerApellido = personas.PrimerApellido,
                PrimerNombre = personas.PrimerNombre,
                ProgramaId = 0,
                RegionalId = 0,
                SegundoApellido = personas.SegundoApellido,
                SegundoNombre = personas.SegundoNombre,
                Telefono = personas.Telefono,
                TipoDocumentoId = personas.TipoDocumentoId,
                TipoPersonaId = personas.TipoPersonaId,
                TipoProgramaId = 0,
                TipoDocumentoDescripcion = tipoDocumento.Descripcion,
                TipoPersonaDescripcion = tipoPersona.Descripcion,

            };

            if (tipoPersona.Descripcion == "Aprendiz")
            {
                Aprendiz aprendiz = db.Aprendizs.Where(a => a.PersonaId == id).FirstOrDefault();
                ProgramaCentro programaCentro = db.ProgramaCentros.Where(pc => pc.ProgramaCentroId == aprendiz.ProgramaCentroId).FirstOrDefault();
                Centro regional = db.Centros.Where(c => c.CentroId == programaCentro.CentroId).FirstOrDefault();

                personaViewEdit.AprendizId = aprendiz.AprendizId;
                personaViewEdit.CentroId = programaCentro.CentroId;
                personaViewEdit.Ficha = aprendiz.Ficha;
                personaViewEdit.ProgramaId = programaCentro.ProgramaId;
                personaViewEdit.RegionalId = regional.RegionalId;
                personaViewEdit.TipoProgramaId = aprendiz.TipoProgramaId;

            }


            return View(personaViewEdit);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            this.CreateTipoDocumento("Cédula de Ciudadanía");
            this.CreateTipoDocumento("Tarjeta de Identidad");
            this.CreateTipoDocumento("Cédula de Extranjería");
            this.CreateTipoDocumento("Pasaporte");
            this.CreateTipoPersona("Aprendiz");
            this.CreateTipoPersona("Instructor");
            this.CreateTipoPersona("Bienestar Al Aprendiz");
            this.CreateTipoPersona("Coordinador Académico");
            this.CreateTipoPersona("Coordinador de Formación");
            this.CreateTipoPersona("Sub Director");
            this.CreateTipoPersona("¿Otro?");
            this.CreateTipoPrograma("Virtual");
            this.CreateTipoPrograma("Presencial");
            this.CreateTipoPrograma("Semi-Presencial");

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                TempData["Errors"] = ex.Message;
                return RedirectToAction("Index", "Home");
            }
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion");
            ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion");
            ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre");
            ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre");
            ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre");
            ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion");

            return View();
        }

        private void CreateTipoPrograma(string TipoProg)
        {
            var TipoPrograma = db.TipoProgramas.Where(Tp => Tp.Descripcion.Equals(TipoProg));

            if (TipoPrograma.Count() == 0)
            {
                var tipoPrograma = new TipoPrograma
                {
                    Descripcion = TipoProg,
                };
                db.TipoProgramas.Add(tipoPrograma);
            }
        }

        private void CreateTipoPersona(string TipoPers)
        {
            var TipoPersona = db.TipoPersonas.Where(tp => tp.Descripcion.Equals(TipoPers));

            if (TipoPersona.Count() == 0)
            {
                var tipoPersona = new TipoPersona
                {
                    Descripcion = TipoPers,
                };
                db.TipoPersonas.Add(tipoPersona);
            }
        }

        private void CreateTipoDocumento(string TipoDoc)
        {
            var TipoDocumento = db.TipoDocumentos.Where(td => td.Descripcion.Equals(TipoDoc));
            var abreviacion = "";
            if (TipoDocumento.Count() == 0)
            {
                if (TipoDoc == "Cédula de Ciudadanía")
                {
                    abreviacion = "CC";
                }

                else if (TipoDoc == "Tarjeta de Identidad")
                {
                    abreviacion = "TI";
                }

                else if (TipoDoc == "Cédula de Extranjería")
                {
                    abreviacion = "CE";
                }

                else if (TipoDoc == "Pasaporte")
                {
                    abreviacion = "PAS";
                }
                else if (TipoDoc == "Documento Nacional de Identificación" || TipoDoc == "DNI")
                {
                    abreviacion = "D.N.I";
                }

                else if (TipoDoc == "Numero Unico Tributario")
                {
                    abreviacion = "NIT";
                }

                else
                {
                    abreviacion = "Doc";
                }


                var tipoDocumento = new TipoDocumento
                {
                    Descripcion = TipoDoc,
                    Abreviacion = abreviacion
                };
                db.TipoDocumentos.Add(tipoDocumento);
            }
        }

        // POST: Personas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PersonaView personaView, int? uno)
        {
            ViewBag.Error1 = "0";
            ViewBag.Error2 = "0";
            ViewBag.Error3 = "0";
            ViewBag.Error4 = "0";
            ViewBag.Error5 = "0";
            ViewBag.Error6 = "0";
            ViewBag.Error7 = "0";
            ViewBag.Error8 = "0";

            var correo = db.Personas.Where(p => p.Correo == personaView.Persona.Correo).Count();
            var cedula = db.Personas.Where(p => p.Documento == personaView.Persona.Documento).Count();

            if (correo > 0)
            {
                ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion", Request.Form["TipoDocumentoId"]);
                ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion", Request.Form["TipoPersonaId"]);
                ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre", Request.Form["RegionalId"]);
                ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre", Request.Form["ProgramaId"]);
                ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre", Request.Form["CentroId"]);
                ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion", Request.Form["TipoProgramaId"]);

                ViewBag.Error7 = "1";

                return View(personaView);
            }

            if (cedula > 0)
            {
                ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion", Request.Form["TipoDocumentoId"]);
                ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion", Request.Form["TipoPersonaId"]);
                ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre", Request.Form["RegionalId"]);
                ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre", Request.Form["ProgramaId"]);
                ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre", Request.Form["CentroId"]);
                ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion", Request.Form["TipoProgramaId"]);

                ViewBag.Error8 = "1";

                return View(personaView);
            }

            if (Request.Form["TipoDocumentoId"] == "" || Request.Form["TipoPersonaId"] == "" || (personaView.Persona.Celular == null && personaView.Persona.Telefono == null))
            {

                if (Request.Form["TipoDocumentoId"] == "")
                {
                    ViewBag.Error1 = "1";
                }

                if (Request.Form["TipoPersonaId"] == "")
                {
                    ViewBag.Error2 = "1";
                }

                if (personaView.Persona.Celular == null && personaView.Persona.Telefono == null)
                {
                    ViewBag.Error3 = "1";

                }

                ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion", Request.Form["TipoDocumentoId"]);
                ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion", Request.Form["TipoPersonaId"]);
                ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre", Request.Form["RegionalId"]);
                ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre", Request.Form["ProgramaId"]);
                ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre", Request.Form["CentroId"]);
                ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion", Request.Form["TipoProgramaId"]);

                return View(personaView);
            }
            else if (ModelState.IsValid)
            {
                if (personaView.TipoDocumento.Descripcion != null)
                {
                    this.CreateTipoDocumento(personaView.TipoDocumento.Descripcion);
                }

                if (personaView.TipoPersona.Descripcion != null)
                {
                    var tipoPersonas = new TipoPersona
                    {
                        Descripcion = personaView.TipoPersona.Descripcion
                    };
                    db.TipoPersonas.Add(tipoPersonas);
                }
                db.SaveChanges();

                var tipoDocumentoId = db.TipoDocumentos.Select(td => td.TipoDocumentoId).Max();
                var tipoPersonaId = db.TipoPersonas.Select(tp => tp.TipoPersonaId).Max();

                var Personas = new Persona
                {
                    Celular = personaView.Persona.Celular,
                    Correo = personaView.Persona.Correo,
                    Direccion = personaView.Persona.Direccion,
                    Documento = personaView.Persona.Documento,
                    Estado = true,
                    PrimerApellido = personaView.Persona.PrimerApellido,
                    PrimerNombre = personaView.Persona.PrimerNombre,
                    SegundoApellido = personaView.Persona.SegundoApellido,
                    SegundoNombre = personaView.Persona.SegundoNombre,
                    Telefono = personaView.Persona.Telefono,
                    TipoDocumentoId = (personaView.TipoDocumento.Descripcion != null ? tipoDocumentoId : Convert.ToInt32(Request.Form["TipoDocumentoId"])),
                    TipoPersonaId = (personaView.TipoPersona.Descripcion != null ? tipoPersonaId : Convert.ToInt32(Request.Form["TipoPersonaId"])),

                };
                db.Personas.Add(Personas);
                db.SaveChanges();

                var id = Request.Form["TipoPersonaId"];
                var TipoPers = db.TipoPersonas.Find(int.Parse(id));


                if (TipoPers.Descripcion == "Aprendiz")
                {

                    if (Request.Form["CentroId"] == "" || Request.Form["ProgramaId"] == "" || Request.Form["TipoProgramaId"] == "")
                    {

                        if (Request.Form["CentroId"] == "")
                        {
                            ViewBag.Error4 = "1";
                        }

                        if (Request.Form["ProgramaId"] == "")
                        {
                            ViewBag.Error5 = "1";
                        }

                        if (Request.Form["TipoProgramaId"] == "")
                        {
                            ViewBag.Error6 = "1";
                        }

                        if (Request.Form["TipoDocumentoId"] == "")
                        {
                            ViewBag.Error1 = "1";
                        }


                        ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion", Request.Form["TipoDocumentoId"]);
                        ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion", Request.Form["TipoPersonaId"]);
                        ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre", Request.Form["RegionalId"]);
                        ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre", Request.Form["ProgramaId"]);
                        ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre", Request.Form["CentroId"]);
                        ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion", Request.Form["TipoProgramaId"]);

                        return View(personaView);
                    }

                    int centroid = int.Parse(Request.Form["CentroId"]);
                    int programaid = int.Parse(Request.Form["ProgramaId"]);

                    var personaid = db.Personas.Where(p => p.Correo == personaView.Persona.Correo)
                                             .Select(pe => pe.PersonaId)
                                             .FirstOrDefault();

                    var programaCentro = db.ProgramaCentros
                                           .Where(pc => pc.CentroId == centroid
                                            && pc.ProgramaId == programaid)
                                           .Select(pcs => pcs.ProgramaCentroId)
                                           .FirstOrDefault();

                    if (programaCentro == 0)
                    {

                        var ProgramasCentros = new ProgramaCentro
                        {
                            CentroId = centroid,
                            ProgramaId = programaid
                        };

                        db.ProgramaCentros.Add(ProgramasCentros);
                        db.SaveChanges();
                    }

                    var programaCentroId = db.ProgramaCentros
                                           .Where(pc => pc.CentroId == centroid &&
                                                  pc.ProgramaId == programaid)
                                           .Select(pcs => pcs.ProgramaCentroId)
                                           .FirstOrDefault();

                    var Aprendiz = new Aprendiz
                    {
                        Ficha = personaView.Aprendiz.Ficha,
                        PersonaId = personaid,
                        ProgramaCentroId = programaCentroId,
                        TipoProgramaId = Convert.ToInt32(Request.Form["TipoProgramaId"]),
                    };
                    db.Aprendizs.Add(Aprendiz);
                    db.SaveChanges();

                }

                else
                {

                    var userContext = new ApplicationDbContext();
                    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
                    var db = new SceysContext();
                    var Username = personaView.Persona.Correo;

                    this.Role("Administrador", userContext);
                    this.Role("Funcionarios", userContext);
                    this.Role("Instructores", userContext);
                    this.Role("Otro", userContext);

                    var userASP = userManager.FindByName(Username);

                    if (userASP == null)
                    {
                        userASP = new ApplicationUser
                        {
                            UserName = Username,
                            Email = personaView.Persona.Correo,
                            PhoneNumber = personaView.Persona.Celular,
                        };
                        userManager.Create(userASP, "Cc." + personaView.Persona.Documento);
                    }

                    if (TipoPers.Descripcion == "Sub Director" ||
                        TipoPers.Descripcion == "Coordinador Académico" ||
                        TipoPers.Descripcion == "Coordinador de Formación")
                    {
                        userManager.AddToRole(userASP.Id, "Administrador");
                    }

                    else if (TipoPers.Descripcion == "Bienestar Al Aprendiz")
                    {
                        userManager.AddToRole(userASP.Id, "Funcionarios");
                    }

                    else if (TipoPers.Descripcion == "Instructor")
                    {
                        userManager.AddToRole(userASP.Id, "Instructores");
                    }

                    else
                    {
                        userManager.AddToRole(userASP.Id, "Otro");
                    }

                }

                return RedirectToAction("Index");
            }


            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion", Request.Form["TipoDocumentoId"]);
            ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion", Request.Form["TipoPersonaId"]);
            ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre", Request.Form["RegionalId"]);
            ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre", Request.Form["ProgramaId"]);
            ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre", Request.Form["CentroId"]);
            ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion", Request.Form["TipoProgramaId"]);

            return View(personaView);
        }


        private void Role(string RoleName, ApplicationDbContext userContext)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));

            if (!roleManager.RoleExists(RoleName))
            {
                roleManager.Create(new IdentityRole(RoleName));
            }
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Persona personas = db.Personas.Find(id);
            if (personas == null)
            {
                return HttpNotFound();
            }

            var personaViewEdit = new PersonaViewEdit
            {
                AprendizId = 0,
                Celular = personas.Celular,
                CentroId = 0,
                Correo = personas.Correo,
                Direccion = personas.Direccion,
                Documento = personas.Documento,
                Ficha = null,
                PersonaId = personas.PersonaId,
                PrimerApellido = personas.PrimerApellido,
                PrimerNombre = personas.PrimerNombre,
                ProgramaId = 0,
                RegionalId = 0,
                SegundoApellido = personas.SegundoApellido,
                SegundoNombre = personas.SegundoNombre,
                Telefono = personas.Telefono,
                TipoDocumentoId = personas.TipoDocumentoId,
                TipoPersonaId = personas.TipoPersonaId,
                TipoProgramaId = 0,
                TipoDocumentoDescripcion = null,
                TipoPersonaDescripcion = null,

            };

            TipoPersona tipoPersona = db.TipoPersonas.Find(personas.TipoPersonaId);

            if (tipoPersona.Descripcion == "Aprendiz")
            {
                Aprendiz aprendiz = db.Aprendizs.Where(a => a.PersonaId == id).FirstOrDefault();
                ProgramaCentro programaCentro = db.ProgramaCentros.Where(pc => pc.ProgramaCentroId == aprendiz.ProgramaCentroId).FirstOrDefault();
                Centro regional = db.Centros.Where(c => c.CentroId == programaCentro.CentroId).FirstOrDefault();

                personaViewEdit.AprendizId = aprendiz.AprendizId;
                personaViewEdit.CentroId = programaCentro.CentroId;
                personaViewEdit.Ficha = aprendiz.Ficha;
                personaViewEdit.ProgramaId = programaCentro.ProgramaId;
                personaViewEdit.RegionalId = regional.RegionalId;
                personaViewEdit.TipoProgramaId = aprendiz.TipoProgramaId;

            }


            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion", personaViewEdit.TipoDocumentoId);
            ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion", personaViewEdit.TipoPersonaId);
            ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre", personaViewEdit.RegionalId);
            ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre", personaViewEdit.ProgramaId);
            ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre", personaViewEdit.CentroId);
            ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion", personaViewEdit.TipoProgramaId);
            return View(personaViewEdit);
        }

        // POST: Personas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PersonaViewEdit personaView)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Error1 = "0";
                ViewBag.Error2 = "0";


                var tipoDocumento = db.TipoDocumentos.Where(td => td.TipoDocumentoId == personaView.TipoDocumentoId).FirstOrDefault();
                var tipoPersona = db.TipoPersonas.Where(tp => tp.TipoPersonaId == personaView.TipoPersonaId).FirstOrDefault();

                if (tipoDocumento.Descripcion == "¿Otro?" || tipoPersona.Descripcion == "¿Otro?")
                {
                    if (tipoDocumento.Descripcion == "¿Otro?")
                    {
                        ViewBag.Error1 = "1";
                    }
                    if (tipoPersona.Descripcion == "¿Otro?")
                    {
                        ViewBag.Error2 = "1";
                    }
                    ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion", personaView.TipoDocumentoId);
                    ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion", personaView.TipoPersonaId);
                    ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre", personaView.RegionalId);
                    ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre", personaView.ProgramaId);
                    ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre", personaView.CentroId);
                    ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion", personaView.TipoProgramaId);
                    return View(personaView);
                }



                Persona persona = new Persona
                {
                    Celular = personaView.Celular,
                    Correo = personaView.Correo,
                    Direccion = personaView.Direccion,
                    Documento = personaView.Documento,
                    Estado = true,
                    PersonaId = personaView.PersonaId,
                    PrimerApellido = personaView.PrimerApellido,
                    PrimerNombre = personaView.PrimerNombre,
                    SegundoApellido = personaView.SegundoApellido,
                    SegundoNombre = personaView.SegundoNombre,
                    Telefono = personaView.Telefono,
                    TipoDocumentoId = personaView.TipoDocumentoId,
                    TipoPersonaId = personaView.TipoPersonaId,
                };

                db.Entry(persona).State = EntityState.Modified;

                if (tipoPersona.Descripcion == "Aprendiz")
                {
                    var programaCentro = db.ProgramaCentros
                                           .Where(pc => pc.CentroId == personaView.CentroId
                                            && pc.ProgramaId == personaView.ProgramaId)
                                           .Select(pcs => pcs.ProgramaCentroId)
                                           .FirstOrDefault();

                    if (programaCentro == 0)
                    {

                        var ProgramasCentros = new ProgramaCentro
                        {
                            CentroId = personaView.CentroId,
                            ProgramaId = personaView.ProgramaId
                        };

                        db.ProgramaCentros.Add(ProgramasCentros);
                        db.SaveChanges();
                    }

                    var programaCentroId = db.ProgramaCentros
                                           .Where(pc => pc.CentroId == personaView.CentroId &&
                                                  pc.ProgramaId == personaView.ProgramaId)
                                           .Select(pcs => pcs.ProgramaCentroId)
                                           .FirstOrDefault();

                    Aprendiz aprendiz = new Aprendiz
                    {
                        AprendizId = personaView.AprendizId,
                        Ficha = personaView.Ficha,
                        PersonaId = personaView.PersonaId,
                        ProgramaCentroId = programaCentroId,
                        TipoProgramaId = personaView.TipoProgramaId,
                    };

                    db.Entry(aprendiz).State = EntityState.Modified;
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion", personaView.TipoDocumentoId);
            ViewBag.TipoPersonaId = new SelectList(db.TipoPersonas.OrderBy(tp => tp.Descripcion), "TipoPersonaId", "Descripcion", personaView.TipoPersonaId);
            ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre", personaView.RegionalId);
            ViewBag.ProgramaId = new SelectList(db.Programas.OrderBy(p => p.Nombre), "ProgramaId", "Nombre", personaView.ProgramaId);
            ViewBag.CentroId = new SelectList(db.Centros.OrderBy(c => c.Nombre), "CentroId", "Nombre", personaView.CentroId);
            ViewBag.TipoProgramaId = new SelectList(db.TipoProgramas.OrderBy(tpr => tpr.Descripcion), "TipoProgramaId", "Descripcion", personaView.TipoProgramaId);
            return View(personaView);
        }

        // GET: Personas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Delete/5

        public ActionResult DeleteConfirmed(int id)
        {
            Persona persona = db.Personas.Find(id);
            persona.Estado = false;
            db.Entry(persona).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Carga la vista para registrar aprendiz por el instructor
        public ActionResult registrarAprendiz() {
            if (User.Identity.IsAuthenticated) {
                ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos.OrderBy(td => td.Descripcion), "TipoDocumentoId", "Descripcion");
                ViewBag.RegionalId = new SelectList(db.Regionals.OrderBy(r => r.Nombre), "RegionalId", "Nombre");
                return View();
            }
            return RedirectToAction("Index", "Home");
        }

        //Metodo para registrar un aprendiz por el instructor
        public JsonResult registrarAprendizXInstructor(PersonaRegistrarAprendizXInstructorView aprendiz)
        {
          if(aprendiz!=null){
                var persona = new Persona {
                    PrimerNombre = aprendiz.PrimerNombre,
                    SegundoNombre = aprendiz.SegundoNombre,
                    PrimerApellido = aprendiz.PrimerApellido,
                    SegundoApellido = aprendiz.SegundoApellido,
                    Correo = aprendiz.Correo,
                    Direccion = aprendiz.Direccion,
                    Celular = aprendiz.Celular,
                    Telefono = aprendiz.Telefono
                };

            db.Personas.Add(persona);
            db.SaveChanges();
            /*
            var nuevoAprendiz = new Aprendiz {
              PersonaId = persona.PersonaId,
              Ficha = aprendiz.Ficha,
              RegionalId = aprendiz.RegionalId,
              CentroId = aprendiz.CentroId
            }

            db.Aprendiz.Add(nuevoAprendiz);
            db.SaveChanges();

            return Json(1, AllowGet);*/
          }
          return Json(0, JsonRequestBehavior.AllowGet);
        }
    }
}
